# Phishing phish

This is an example project to show how phishing can possibly work. 
(Side note This project is only for test purposes.)

## Requirements 
- Python
- Aws account
- Terraform
- Java sdk (Maven)
- Email address (to send mails via smtp)

## Setup
### Infrastructure setup via AWS and Terraform

Amazon web services are used as cloud infrastructure. This way we don't need to worry about on-premise servers and databases. 
Via, Terraform we apply our resources to the cloud. (Terraform is an infrastructure as code [IAC] tool  to easily deploy resources to the cloud.)

#### [api gateway](https://aws.amazon.com/api-gateway/) 

Used for exposing api endpoints. In this project we have 3 endpoints.

- Clicking the link in the phishing mail:
`curl --location 'https://{{api-id}}.execute-api.eu-west-1.amazonaws.com/api?uid=12345'`
- Pressing the submit button on the phishing website: 
`curl --location 'https://{{api-id}}.execute-api.eu-west-1.amazonaws.com/api' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'uid=12345'`
- Getting the results of all phished people: 
`curl --location 'https://{{api-id}}.execute-api.eu-west-1.amazonaws.com/api/all'`

#### [lambda](https://aws.amazon.com/lambda/) 

Behind the api gateway we have some lambda functions.

AWS Lambda is a serverless compute service provided by Amazon Web Services (AWS) that allows users to run code without provisioning or managing servers.
Another advantage is that you don't need a server that is powered on all day. 
You only pay for what you use. You are charged based on the number of requests for your functions and the duration it takes for your code to execute.

The code is written in Python and handles the queries to the database:

- Clicking the link in the phishing mail:
  The [click.py](backend%2Fsrc%2Fmessages%2Fclick.py) stores an uid in the db and returns the html for the fake phishing site.
- Pressing the submit button on the phishing website:
  The [submit.py](backend%2Fsrc%2Fmessages%2Fsubmit.py) stores an uid in the db with `hasPushedSubmit `set to true. 
- Getting the results of all phished people:
  The [all.py](backend%2Fsrc%2Fmessages%2Fall.py) Returns all the db records as json.

#### [dynamodb](https://aws.amazon.com/dynamodb/) 

Amazon DynamoDB is a fully managed, serverless, key-value NoSQL database designed to run high-performance applications at any scale.

The database for this project is easy. We have a primary key `uid` and a field `hasPushedSubmit` to see if a person has clicked a button.  
The uid is the reference that needs to be sent in the phishing email. This is our reference to the person's email address that has been phished.

### Sending phishing emails via cli-phisher

I started from an existing project [cli-phisher](https://github.com/blark/cli-phisher). This is a cli script that sends the emails containing a malicious website.

(The project was a little outdated, so I did some changes to make it work again)

- [config.yml](cli-phisher%2Fconfig.yml) contains the configuration of the smtp email used to send the emails.
- [email.md](cli-phisher%2Femail.md) contains the configuration of the phishing email you want to send.
- [test_targets.txt](cli-phisher%2Ftest_targets.txt) contains all the email addresses of target you want to phish.
- [test.log](cli-phisher%2Ftest.log) contains the log file when running the script. This file can be used to reference the uid with the right email address.

Read the [cli-phisher README.md](cli-phisher%2FREADME.md) to get more info how to work with the script.

### Fetch results

To reference the uid with the right email address, I created a script in Java  [Main.java](backend%2Fsrc%2Fresult%2Fsrc%2Fmain%2Fjava%2Fbe%2Farneckx%2Fphishing%2FMain.java) that reads the [log file](cli-phisher%2Ftest.log) and fetches the results from our api `https://{{api-id}}.execute-api.eu-west-1.amazonaws.com/api/all`.
With this information we can find out the results of who has been a victim of our phishing trap.

The output is json:
```
[
  {
    "email": "test-1@email.com",
    "uid": "dGVzdC0xQGVtYWlsLmNvbQo=",
    "hasPushedSubmit": true
  },
  {
    "email": " test-2@email.com",
    "uid": "dGVzdC0yQGVtYWlsLmNvbQo=",
    "hasPushedSubmit": false
  }
]
```

You can run the script via following commands inside the [result](backend%2Fsrc%2Fresult) folder:
```
mvn clean install
java -jar target/result-0.0.1-SNAPSHOT-shaded.jar
```

[I realised this too late, but the uid is a base64 encoded email address. So, we don't really need this script to find out the mail address for the uid.]

## Todos
The following todos need to be replaced when going through the instructions in the section below.

- {{todo-1}} => The api id of the aws api gateway resource.
- {{todo-2}} => Fill in the credentials of your mail account (You can also change the host and port if you don't have a gmail address)
- {{todo-3}} => The sender's name. Targets will see this name as the sender of the mail.
- {{todo-4}} => Fill in all the email targets separated with a new line.
- {{todo-5}} => The absolute path of the [test.log](cli-phisher%2Ftest.log) file

## Instructions
- Run the [build.sh](backend%2Fsrc%2Fmessages%2Fbuild.sh) script to create a zip for the python code.
- Run `terraform apply` inside the [infra](backend%2Finfra) folder. (You will need an aws account and set the aws credentials in the `~/.aws/credentials` file)
- As output, you will get the full phishing_api url that can be used to fill in the {{todo-1}} placeholders. 
- Replace all {{todo-1}} placeholders.
- Run the [build.sh](backend%2Fsrc%2Fmessages%2Fbuild.sh) again because the {{todo-1}} placeholder has been changed.
- Run `terraform apply` again because the {{todo-1}} placeholder has been changed.
- Replace the remaining {{todos}}.
- Run the script via the command `python3 cli-phisher.py --send email.md`. All the mails will be sent now.
- Wait for the targets to interact to the email or not.
- Fetch the results via the api call or by running the result script:
  - Go to the [result](backend%2Fsrc%2Fresult) folder.
  - Run `mvn clean install`
  - Run `java -jar target/result-0.0.1-SNAPSHOT-shaded.jar`

## Difficulties

I had some issues to configure the mail address to send the emails with. Google has some strict security policies, and it is not that straightforward to send emails via python.

- I used a Google Workspace account and enabled less secure app access.
- I generated an app password.

I followed some troubleshooting pages:
- https://docs.bitnami.com/aws/faq/troubleshooting/troubleshoot-smtp-gmail/
- https://support.google.com/accounts/answer/6010255?hl=en