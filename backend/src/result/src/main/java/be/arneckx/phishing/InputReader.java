package be.arneckx.phishing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public record InputReader() {

    public Map<String, String> read(String inputFile) {
        return readFile(inputFile);
    }

    private Map<String, String> readFile(String inputFile) {
        Map<String, String> lookupMap = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getFile(inputFile)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains("Sent email to")){
                    line = line.substring(line.indexOf("Sent email to") + "Sent email to".length());
                    String[] split = line.split(" with id ");
                    lookupMap.put(split[1], split[0]);
                }
            }
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        return lookupMap;
    }

    private InputStream getFile(String inputFile) throws IOException {
        File file = new File(inputFile);
        return new FileInputStream(file);
    }
}
