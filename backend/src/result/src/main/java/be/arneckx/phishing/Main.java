package be.arneckx.phishing;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;

public class Main {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        Map<String, String> emailAddressWithKey = new be.arneckx.phishing.InputReader().read("{{todo-5}}/phising/cli-phisher/test.log");

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(new URI("{{todo-1}}.amazonaws.com/api/all"))
                .GET()
                .build();

        List<Result> results = objectMapper.readValue(
                HttpClient.newBuilder().build().send(httpRequest, HttpResponse.BodyHandlers.ofByteArray()).body(),
                new TypeReference<>() {
                }
        );
        List<ResultWithMail> finalResults = results.stream()
                .map(result -> new ResultWithMail(
                        emailAddressWithKey.get(result.uid),
                        result.uid,
                        result.hasPushedSubmit
                ))
                .toList();
        System.out.println(objectMapper.writeValueAsString(finalResults));
    }

    record Result(String uid, boolean hasPushedSubmit) {
    }

    record ResultWithMail(String email, String uid, boolean hasPushedSubmit) {
    }
}
