import boto3
from boto3.dynamodb.conditions import Key

def apply(event, context):
    uid = event['queryStringParameters']['uid']
    print(f'## UID -> {uid}')

    dynamodb = boto3.resource('dynamodb')

    table = dynamodb.Table('messages')
    db_response = table.query(
        KeyConditionExpression=Key('uid').eq(uid)
    )
    items = db_response["Items"]
    if len(items) == 0:
        table.put_item(
            Item={
                'uid': uid,
                'hasPushedSubmit': 'false'
            }
        )

        print(f'## CREATED message with id: {uid}')

    response = {
        "statusCode": 200,
        "body": page_for(uid),
        "headers": {'content-type': 'text/html', 'Access-Control-Allow-Origin': '*'}
    }

    return response

def page_for(uid):
    return f'''
        <!DOCTYPE html>
        <html>
        <body>
        
        <h1>My very real website</h1>
        
        <div>
            <label for="username">Username:</label>
            <input type="text" id="username" name="username">
        </div>
        <div>
            <label for="password">Password:</label>
            <input type="text" id="password" name="password">
        </div>
        <form accept-charset=utf-8 action="{{todo-1}}.amazonaws.com/api" method="post">
            <div>
                <input type="text" id="uid" name="uid" value="{uid}" hidden>
            </div>
            <button type="submit">Login</button>
        </form>
        
        </body>
        </html>
    '''