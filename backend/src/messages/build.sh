#!/usr/bin/env bash

mkdir ./build

rm ./build/all.zip
rm ./build/click.zip
rm ./build/submit.zip

zip -r ./build/all.zip ./all.py ./build
zip -r ./build/click.zip ./click.py ./build
zip -r ./build/submit.zip ./submit.py ./build
