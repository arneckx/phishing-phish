import json
import boto3

def apply(event, context):

    dynamodb = boto3.resource('dynamodb')

    table = dynamodb.Table('messages')

    db_response = table.scan()

    items = db_response["Items"]
    if len(items) > 0:
        messages = []
        for item in items:
            messages.append(message_of(item))
        response = {
            "statusCode": 200,
            "body": json.dumps(messages),
            "headers": {'content-type': 'application/json', 'Access-Control-Allow-Origin': '*'}
        }
    else:
        print("no messages found")
        response = {
            "statusCode": 200,
            "body": json.dumps([]),
            "headers": {'content-type': 'application/json', 'Access-Control-Allow-Origin': '*'}
        }

    return response


def message_of(item):
    return {"uid": item["uid"], "hasPushedSubmit": item["hasPushedSubmit"]}
