import boto3


def apply(event, context):
    print(event)
    body = str(event['body'])
    print(f'## BODY {body}')

    dynamodb = boto3.resource('dynamodb')

    table = dynamodb.Table('messages')
    uid = body.split('=')[1]
    table.put_item(
        Item={
            'uid': uid,
            'hasPushedSubmit': 'true'
        }
    )

    print(f'## CREATED push message with id: {uid}')

    response = {
        "statusCode": 200,
        "body": "",
        "headers": {'Access-Control-Allow-Origin': '*'}
    }

    return response
