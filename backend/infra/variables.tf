variable "env" {
  type = string
}
variable "all_build_path" {
  type        = string
  description = "Path of zipped code for all function."
}
variable "click_build_path" {
  type        = string
  description = "Path of zipped code for click function."
}
variable "submit_build_path" {
  type        = string
  description = "Path of zipped code for submit function."
}
