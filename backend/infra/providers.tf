provider "aws" {

  default_tags {
    tags = {
      Owner       = "arneckx"
      Project     = "phishing"
      Team        = "arneckx"
      Terraform   = "phishing/backend/infra"
      Environment = var.env
    }
  }
}
