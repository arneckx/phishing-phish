resource "aws_dynamodb_table" "messages_dynamodb_table" {
  name         = "messages"
  billing_mode = "PAY_PER_REQUEST"

  hash_key  = "uid"

  attribute {
    name = "uid"
    type = "S"
  }
}
