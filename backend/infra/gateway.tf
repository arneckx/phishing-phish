data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

resource "aws_api_gateway_rest_api" "phishing_api" {
  name = "phishing_api"
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.phishing_api.id

  triggers = {
    redeployment = sha1(join(",", concat(
      [
        jsonencode(aws_api_gateway_resource.all)
      ]
    )))
  }

  depends_on = [
    aws_api_gateway_integration.click,
    aws_api_gateway_integration.submit,
    aws_api_gateway_integration.all,
    aws_api_gateway_method_response.response_200
  ]
}

resource "aws_api_gateway_method" "all" {
  rest_api_id   = aws_api_gateway_rest_api.phishing_api.id
  resource_id   = aws_api_gateway_resource.all.id
  http_method   = "GET"
  authorization = "NONE"

  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_method" "click" {
  rest_api_id   = aws_api_gateway_rest_api.phishing_api.id
  resource_id   = aws_api_gateway_rest_api.phishing_api.root_resource_id
  http_method   = "GET"
  authorization = "NONE"

  request_parameters = {
    "method.request.path.proxy" = true,
    "method.request.querystring.uid" = true
  }
}

resource "aws_api_gateway_method" "submit" {
  rest_api_id   = aws_api_gateway_rest_api.phishing_api.id
  resource_id   = aws_api_gateway_rest_api.phishing_api.root_resource_id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "all" {
  rest_api_id             = aws_api_gateway_rest_api.phishing_api.id
  resource_id             = aws_api_gateway_resource.all.id
  http_method             = aws_api_gateway_method.all.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.all.invoke_arn
}

resource "aws_api_gateway_integration" "submit" {
  rest_api_id             = aws_api_gateway_rest_api.phishing_api.id
  resource_id             = aws_api_gateway_rest_api.phishing_api.root_resource_id
  http_method             = aws_api_gateway_method.submit.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.submit.invoke_arn
}

resource "aws_api_gateway_integration" "click" {
  rest_api_id             = aws_api_gateway_rest_api.phishing_api.id
  resource_id             = aws_api_gateway_rest_api.phishing_api.root_resource_id
  http_method             = aws_api_gateway_method.click.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.click.invoke_arn
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.phishing_api.id
  resource_id = aws_api_gateway_rest_api.phishing_api.root_resource_id
  http_method = aws_api_gateway_method.click.http_method
  status_code = "200"

  response_parameters = { "method.response.header.Content-Type" = true }
}

resource "aws_api_gateway_stage" "api" {
  deployment_id = aws_api_gateway_deployment.this.id
  rest_api_id   = aws_api_gateway_rest_api.phishing_api.id
  stage_name    = "api"
}

resource "aws_api_gateway_resource" "all" {
  rest_api_id = aws_api_gateway_rest_api.phishing_api.id
  parent_id   = aws_api_gateway_rest_api.phishing_api.root_resource_id
  path_part   = "all"
}

resource "aws_iam_role_policy_attachment" "apigateway_cloudwatch" {
  role       = aws_iam_role.iam_for_api_gateway.id
  policy_arn = data.aws_iam_policy.api_gateway_cloudwatch.arn
}

resource "aws_api_gateway_account" "events_apigateway" {
  cloudwatch_role_arn = aws_iam_role.iam_for_api_gateway.arn
}

resource "aws_cloudwatch_log_group" "this" {
  name = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.phishing_api.id}/${aws_api_gateway_stage.api.stage_name}"
}

resource "aws_iam_role" "iam_for_api_gateway" {
  name = "iam_for_api_gateway"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "aws_iam_policy" "api_gateway_cloudwatch" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
}

resource "aws_lambda_permission" "apigw_lambda_all" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.all.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.phishing_api.id}/*"
}

resource "aws_lambda_permission" "apigw_lambda_click" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.click.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.phishing_api.id}/*"
}

resource "aws_lambda_permission" "apigw_lambda_submit" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.submit.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.phishing_api.id}/*"
}