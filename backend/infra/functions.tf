resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "all" {
  filename      = var.all_build_path
  function_name = "all"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "all.apply"

  source_code_hash = filebase64sha256(var.all_build_path)

  runtime = "python3.8"
}

resource "aws_lambda_function" "click" {
  filename      = var.click_build_path
  function_name = "click"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "click.apply"

  source_code_hash = filebase64sha256(var.click_build_path)

  runtime = "python3.8"
}

resource "aws_lambda_function" "submit" {
  filename      = var.submit_build_path
  function_name = "submit"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "submit.apply"

  source_code_hash = filebase64sha256(var.submit_build_path)

  runtime = "python3.8"
}

data "aws_iam_policy_document" "lambda_logging" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:aws:logs:*:*:*"]
  }
}

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"
  policy      = data.aws_iam_policy_document.lambda_logging.json
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

resource "aws_iam_role_policy" "dynamodb-lambda-policy" {
  name   = "dynamodb_lambda_policy"
  role   = aws_iam_role.iam_for_lambda.id
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "dynamodb:PutItem",
          "dynamodb:Query",
          "dynamodb:Scan",
        ],
        "Resource" : aws_dynamodb_table.messages_dynamodb_table.arn
      }
    ]
  })
}